#!/bin/bash -x
#===============================================================================
#
#          FILE: build_and_install.sh
# 
#         USAGE: ./build_and_install.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Petr Lautrbach <plautrba@redhat.com>
#  ORGANIZATION: 
#       CREATED: 12/09/2016 09:19
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

cd /usr/local/src/selinux.git

make CFLAGS="`rpm --eval '%{optflags}'`" DESTDIR=/usr/local/selinux LIBDIR=/usr/local/selinux/usr/lib64 SHLIBDIR=/usr/local/selinux/lib64 install install-pywrap relabel 2>&1 > /usr/local/src/build-selinux.log

export LD_LIBRARY_PATH=/usr/local/selinux/usr/lib64/:/usr/local/selinux/lib64/

rpm -qa libsepol\* libselinux\* libsemanage\* policycoreutils\* checkpolicy\* secilc\* setools\* | sed 's/\(.*\)-[^-]*-[^-]*/\1/' | xargs rpm -e --nodeps

make CFLAGS="`rpm --eval '%{optflags}'`" LIBDIR=/usr/lib64 SHLIBDIR=/lib64 install install-pywrap relabel

unset LD_LIBRARY_PATH

cd /usr/local/src/setools.git
git am /usr/local/src/0001-Fix-build.patch

python setup.py build 2>&1 > /usr/local/src/build-setools.log
python setup.py install 2>&1 >> /usr/local/src/build-setools.log

